# perceive

#### 介绍
钉钉群通知

#### 安装教程

```shell
composer require fmmm/dingding
```
发布配置文件
```shell
php artisan vendor:publish --provider="Fmmm\Dingding\Support\DingServiceProvider"
```

#### 使用说明
##### 异常通知
```php
public function register()
{
    $this->reportable(function (Exception $e) {
        DingHelper::exceptionNotify($e);
    });
}

```
