<?php

return [

    "token" => env('DING_TOKEN', false),

    "secret" => env('DING_SECRET', false)
];
