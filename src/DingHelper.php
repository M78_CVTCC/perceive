<?php

namespace Fmmm\Dingding;


use Fmmm\Dingding\Support\DingExceptionNotificationJob;

class DingHelper
{
    public static function exceptionNotify(\Throwable $e){
        dispatch(new DingExceptionNotificationJob($e->getMessage()));
    }
}
