<?php

namespace Fmmm\Dingding\Support;

use Fmmm\Dingding\DingDing;
use Illuminate\Support\Facades\Facade;

class Ding extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return DingDing::class;
    }
}
