<?php

namespace Fmmm\Dingding\Support;

use Fmmm\Dingding\DingDing;
use Illuminate\Support\ServiceProvider;

class DingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DingDing::class,function ($app){
            return DingDing::getInstance();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/dingding.php' => config_path('dingding.php')
        ]);
    }
}
