<?php

namespace Fmmm\Dingding;

class DingDing
{

    private static $token;
    private static $secret;
    private static $instance;

    private function __construct()
    {
        self::$token = config('dingding.token');
        self::$secret = config('dingding.secret');
    }

    public static function getInstance(): DingDing
    {
        if (self::$instance instanceof DingDing){
            return self::$instance;
        }
        return self::$instance = new static();
    }

    public function exceptionNotification(string $exception,array $at = []){
        $text = config('app.name')."\n";
        $text .= "异常:\n";
        $text .= $exception;
        $this->groupNotification($text,$at);
    }

    private function groupNotification($msg,array $at = []){
        $url = 'https://oapi.dingtalk.com/robot/send?access_token='.self::$token;
        if (list($timestamp,$sign) = $this->encryption()){
            $url .= '&'.http_build_query([
                    'timestamp' => $timestamp,
                    'sign' => $sign
                ]);
        }

        $response = $this->request($url,[
            'msgtype' => 'text',
            'text' => [
                'content' => $msg
            ],
            'at' => $at
        ]);
    }

    private function encryption(): ?array
    {
        if (!self::$secret){
            return null;
        }
        $timestamp = time().rand(100,999);
        $result = hash_hmac('sha256',$timestamp."\n".self::$secret,self::$secret,true);
        return [$timestamp,base64_encode($result)];
    }

    private function request(string $url,array $data){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array ('Content-Type: application/json;charset=utf-8'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // 线下环境不用开启curl证书验证, 未调通情况可尝试添加该代码
        // curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return json_decode($data);
    }
}
