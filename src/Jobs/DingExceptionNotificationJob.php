<?php

namespace Fmmm\Dingding\Support;

use Fmmm\Dingding\DingDing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DingExceptionNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $exception;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($exceptionMsg)
    {
        $this->exception = $exceptionMsg;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DingDing $ding)
    {
        $ding->exceptionNotification($this->exception);
    }
}
